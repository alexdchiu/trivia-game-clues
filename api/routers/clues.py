from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg
from routers.categories import CategoryOut

router = APIRouter()

class ClueOut(BaseModel):
  id: int
  # row_index: int
  # column_index: int
  answer: str
  question: str
  value: int
  # game_id: int
  # board_index: int
  category: CategoryOut
  invalid_count: int
  canon: bool


class Message(BaseModel):
  message: str


class Clues(BaseModel):
  page_count: int
  clues: list[ClueOut]


@router.get(
  "/api/clues/{clue_id}", 
  # response_model=ClueOut,
  responses={404: {"model": Message}}
)
def get_clue(
  clue_id: int, 
  response: Response
  ):
  with psycopg.connect() as conn:
    with conn.cursor() as cur:
      result = cur.execute(
        """
        SELECT json_build_object(
          'id', clues.id,
          'answer', clues.answer,
          'question', clues.question,
          'value', clues.value,
          'invalid_count', clues.invalid_count,
          'category', json_build_object(
            'id', category.id,
            'title', category.title,
            'canon', category.canon
          ),
          'canon', clues.canon
        )
        FROM clues
        INNER JOIN categories AS category ON category.id = clues.category_id
        WHERE clues.id = %s
        """,
        [clue_id],
      ).fetchone()

      if result is None:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": "Clue does not exist"}
      
      else:
        return result
        
      # cur.execute(
      #   f"""
      #   SELECT clues.id
      #         , clues.answer
      #         , clues.question
      #         , clues.value
      #         , clues.invalid_count
      #         , clues.canon
      #         , categories.id
      #         , categories.title
      #         , categories.canon
      #   FROM clues
      #   JOIN categories
      #     ON clues.category_id = categories.id
      #   WHERE clues.id = %s
      # """,
      #   [clue_id],
      # )
      # row = cur.fetchone()
      # # return row


      # if row is None:
      #   response.status_code = status.HTTP_404_NOT_FOUND
      #   return {"message": "Clue does not exist"}

      # category = {
      #   "id": row[6],
      #   "title": row[7],
      #   "canon": row[8],
      # }

      # clue = {
      #   "id": row[0],
      #   "answer": row[1],
      #   "question": row[2],
      #   "value": row[3],
      #   "invalid_count": row[4],
      #   "category": category,
      #   "canon": row[5],
      # }

      # # record = {}
      # # for i, column in enumerate(cur.description):
      # #   record[column.name] = row[i]
      # # return record
      # return clue


@router.get(
  "/api/random-clue", 
  # response_model=ClueOut,
  # responses={404: {"model": Message}}
)
def get_random_clue(
  response: Response
  ):
  with psycopg.connect() as conn:
    with conn.cursor() as cur:
      result = cur.execute(
        """
        SELECT json_build_object(
          'id', clues.id,
          'answer', clues.answer,
          'question', clues.question,
          'value', clues.value,
          'invalid_count', clues.invalid_count,
          'category', json_build_object(
            'id', category.id,
            'title', category.title,
            'canon', category.canon
          ),
          'canon', clues.canon
        )
        FROM clues
        INNER JOIN categories AS category ON category.id = clues.category_id
        ORDER BY random()
        LIMIT 1;
        """,
      ).fetchone()

      if result is None:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": "Clue does not exist"}
      
      else:
        return result


@router.get(
  "/api/clues", 
  # response_model=ClueOut
)
def clues_list(page: int = 0):
  with psycopg.connect() as conn:
    with conn.cursor() as cur:
      clues = cur.execute(
        """
        SELECT json_build_object(
          'id', clues.id,
          'answer', clues.answer,
          'question', clues.question,
          'value', clues.value,
          'invalid_count', clues.invalid_count,
          'category', json_build_object(
            'id', category.id,
            'title', category.title,
            'canon', category.canon
          ),
          'canon', clues.canon
        )
        FROM clues
        INNER JOIN categories AS category ON category.id = clues.category_id
        ORDER BY clues.id
        LIMIT 100 OFFSET %s
        """, [page * 100],
      ).fetchall()

      clues_list = []

      for clue in clues:
        clues_list.append(clue[0])
    
    result = {}
    result['clues'] = clues_list
    return result


@router.delete(
  "/api/clues/{clue_id}",
  response_model=Message,
)
def remove_clue(clue_id: int, response: Response):
  with psycopg.connect() as conn:
    with conn.cursor() as cur:
      try:
        cur.execute(
          """
          DELETE FROM clues
          WHERE id = %s;
          """,
          [clue_id],
        )
        return {
          "message": "Successfully deleted clue",
        }
      
      except psycopg.errors.ForeignKeyViolation:
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {
            "message": "Cannot delete clue because of Foreign Key",
        }