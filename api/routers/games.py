from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg


router = APIRouter()


class GameOut(BaseModel):
  id: int
  episode_id: int
  aired: str
  canon: bool


class GameWithAmountWon(GameOut):
  total_amount_won: int | None


@router.get(
  "/api/games/{game_id}",
  # response_model=GameOut,
)
def get_game(
  game_id: int,
  response: Response,
  ):
  with psycopg.connect() as conn:
    with conn.cursor() as cur:
      result = cur.execute(
        f"""
        SELECT json_build_object(
              'id', games.id,
              'episode_id', games.episode_id,
              'aired', games.aired,
              'canon', games.canon,
              'total_amount_won', SUM(clues.value)
              )                
        FROM games   
        LEFT OUTER JOIN clues ON (games.id = clues.game_id)        
        WHERE games.id = %s
        GROUP BY games.id
        """,
        [game_id],
      ).fetchone()

      result = result[0]

      if result is None:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": "Game does not exist"}

      else:
        return result


@router.post(
  "/api/custom-games",
)
def create_game():
  with psycopg.connect() as conn:
    with conn.cursor() as cur:
      random_clues = cur.execute(
        f"""
        SELECT json_build_object(
          'id', clues.id,
          'answer', clues.answer,
          'question', clues.question,
          'value', clues.value,
          'invalid_count', clues.invalid_count,
          'category', json_build_object(
            'id', category.id,
            'title', category.title,
            'canon', category.canon
          ),
          'canon', clues.canon
        )
        FROM clues
        INNER JOIN categories AS category ON category.id = clues.category_id
        ORDER BY random()
        LIMIT 30;
        """
      ).fetchall()

      random_clues_list = []

      for clue in random_clues:
        random_clues_list.append(clue[0])

      with conn.transaction():
        cur.execute(
          f"""
          INSERT INTO game_definitions (created_on)
          VALUES (CURRENT_TIMESTAMP);
          """
        )
        gd_ids = cur.execute(
          f"""
          SELECT gd.id, gd.created_on
          FROM game_definitions as gd
          ORDER BY gd.created_on DESC
          """
        ).fetchone()
        gd_id = gd_ids[0]
        gd_created_on = gd_ids[1]
        for clue in random_clues_list:
          clue_id = clue['id']
          cur.execute(
            f"""
            INSERT INTO game_definition_clues (game_definition_id, clue_id)
            VALUES (%s, %s)
            """,
            [gd_id, clue_id]
          )
    
        return {
          "id": gd_id,
          "created_on": gd_created_on,
          "clues": random_clues_list
          }


@router.get(
  "/api/custom-games/{custom_game_id}",
)
def get_custom_game(
  custom_game_id: int,
  # response: Response,
  ):
  with psycopg.connect() as conn:
    with conn.cursor() as cur:

      clues = cur.execute(
        """
        SELECT json_build_object(
        'id', clues.id,
        'answer', clues.answer,
        'question', clues.question,
        'value', clues.value,
        'invalid_count', clues.invalid_count,
        'category', json_build_object(
          'id', category.id,
          'title', category.title,
          'canon', category.canon
        ),
        'canon', clues.canon
      )
      FROM game_definitions AS gd
      LEFT JOIN game_definition_clues AS gdc ON (gd.id = gdc.game_definition_id) 
      LEFT JOIN clues ON (gdc.clue_id = clues.id)
      LEFT JOIN categories AS category ON category.id = clues.category_id
      WHERE gd.id = %s 
        """,
        [custom_game_id]
      ).fetchall()

      clues_list = []
      for clue in clues:
        clues_list.append(clue[0])

      gd_data = cur.execute(
        """
        SELECT json_build_object(
          'id', gd.id,
          'created_on', gd.created_on
          )
        FROM game_definitions as gd
        WHERE gd.id = %s
        """,
        [custom_game_id],
      ).fetchall()
      
      result = gd_data[0][0]
      result['clues'] = clues_list

      return result
